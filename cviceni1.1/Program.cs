﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace cviceni1._1
{
    class Program
    {
        class Seznam
        {
            public int data;
            public Seznam next;
        }

        static Seznam Create(int hodnota)
        {
            Seznam s = new Seznam();
            s.data = hodnota;
            s.next = null;
            return s;
        }

        static Seznam ConvertArray(int[] pole)
        {
            Seznam prvek, novy = null, zacatek = null;

            if (pole.Length == 0) return null;

            for (int i = 0; i < pole.Length; i++)
            {
                prvek = Create(pole[i]);
                if (i == 0)
                    zacatek = prvek;
                else
                    novy.next = prvek;
                novy = prvek;
            }
            return zacatek;
        }

        static void Print(Seznam s)
        {
            Seznam tisk = s;
            while (tisk != null)
            {
                if (tisk.next == null)
                    Console.Write(tisk.data);
                else
                    Console.Write(tisk.data + " -> ");
                tisk = tisk.next;
            }
            Console.WriteLine();
        }

        static void Save(string cesta, Seznam s)
        {
            StreamWriter sw = new StreamWriter(cesta);
            Seznam tisk = s;

            while (tisk != null)
            {
                sw.WriteLine(tisk.data);
                tisk = tisk.next;
            }
            sw.Close();
        }

        static Seznam Load(string cesta)
        {
            Seznam prvek, novy = null, zacatek = null;
            StreamReader sr = new StreamReader(cesta);
            int cislo;

            while ((cislo = Convert.ToInt32(sr.ReadLine())) != '\0')
            {
                prvek = Create(cislo);
                if (zacatek == null)
                    zacatek = prvek;
                else
                    novy.next = prvek;
                novy = prvek;
            }

            return zacatek;
        }

        static int Count(Seznam s, int number)
        {
            int pocet = 0;

            while (s != null)
            {
                if (s.data == number) pocet++;
                s = s.next;
            }

            return pocet;
        }

        static Seznam DeleteLast(Seznam s, int number)
        {
            Seznam prev = null, zacatek = null;

            zacatek = s;

            if (s.next == null) return null;

            while (s.next != null)
            {
                if (prev == null && s.data == number)
                    prev = s;
                if (s.next.data == number)
                    prev = s;
                s = s.next;
            }

            if (zacatek == prev)
                zacatek = prev.next;
            prev.next = prev.next.next;

            return zacatek;
        }

        static Seznam DeleteMax(Seznam s)
        {
            Seznam zacatek = s, max = s;

            if (s.next == null) return null;

            while (s.next != null)
            {
                if (s.next.data > max.data)
                    max = s;
                s = s.next;
            }

            if (zacatek == max)
                zacatek = max.next;
            max.next = max.next.next;

            return zacatek;
        }

        static Seznam Reverse(Seznam s)
        {
            Seznam zacatek = s, tmp = null, dalsi = null;

            while (s != null)
            {
                if (s.next == null)
                    zacatek = s;
                if (tmp == null)
                {
                    tmp = s;
                    s = s.next;
                    tmp.next = null;
                }
                dalsi = s;
                s = s.next;
                dalsi.next = tmp;
                tmp = dalsi;
            }

            return zacatek;
        }

        static Seznam SwapFirstLast(Seznam s)
        {
            Seznam zacatek = s, prev = s;

            if (s.next == null) return s;

            if (s.next.next == null)
            {
                s = s.next;
                prev = zacatek;
                s.next = prev;
                prev.next = null;
                return s;
            }

            while (prev.next.next != null)
                s = prev = prev.next;
            s = s.next;
            prev.next = zacatek;
            s.next = zacatek.next;
            zacatek.next = null;

            return s;
        }

        static void Main(string[] args)
        {
            int[] pole = { 1055, 2, 29, 8, 7, 15, 29, 8, 22, 6, 29 };
            Seznam s1 = ConvertArray(pole);
            Console.WriteLine("ConvertArray");
            Print(s1);
            Save("spojak.txt", s1);
            Seznam s2 = Load("spojak.txt");
            Console.WriteLine("Load");
            Print(s2);
            int hledane =  29;
            int pocet = Count(s1, hledane); // 3
            Console.WriteLine("Pocet vsech cisel {0} je {1}.", hledane, pocet);
            s1 = DeleteLast(s1, 8);
            Console.WriteLine("Delete last");
            Print(s1);
            s1 = DeleteMax(s1);
            Console.WriteLine("Delete max");
            Print(s1);
            s1 = Reverse(s1);
            Console.WriteLine("Reverse");
            Print(s1);
            // s1 = Sort(s1);
            // 29->29->29->22->15->8->7->6->2
            s1 = SwapFirstLast(s1);
            Console.WriteLine("Swap first and last");
            Print(s1);
            // 2->29->29->22->15->8->7->6->29
            // s1 = SwapSecondPrenultimate(s1);
            // 2->6->29->22->15->8->7->29->29
            Console.Read();
        }
    }
}
